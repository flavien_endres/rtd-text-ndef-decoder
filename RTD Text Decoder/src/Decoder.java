import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Decoder {
	
	static final int NDEFHEADER_POS = 0;
	static final int RECORDNAMELENGTH_POS = 1;
	static final int PAYLOADLENGTH_POS = 2;
	static final int RECORDNAME_POS = 3;
	
	static private StringBuilder appendHeader( StringBuilder output, int ndefHeader, int recordNameLength, int payloadLength, String recordName) {
		
		output.append( "NDEF Header : " ).append( Integer.toBinaryString(ndefHeader) ).append("\n");
		output.append( "Record name length : " ).append(recordNameLength).append("\n");
		output.append( "Payload length : " ).append(payloadLength).append("\n");
		output.append( "Record name : " ).append(recordName).append("\n");
		
		return output;
		
	}
	
	static private String decimalToText( ArrayList<Integer> list, int start, int end ) {
		
		StringBuilder txt = new StringBuilder();
		
		for( int i = start; i < end; i++ ) {
			txt.append( Character.toString( list.get( i ) ) );

		}
		
		return txt.toString();
		
	}
	
	public String decode( Trame trame ) {
		
		String recordName = "";
		
		String ndef = trame.getNDEF();
		StringBuilder output_description = new StringBuilder();
		ndef.replaceAll(" ", "");
		
		ArrayList<Integer> decimalMSG = parseHexa( ndef );
		
		int ndefHeader = decimalMSG.get( NDEFHEADER_POS );
		int recordNameLength = decimalMSG.get( RECORDNAMELENGTH_POS );
		int payloadLength = decimalMSG.get( PAYLOADLENGTH_POS );
		
		int i = 0;
		int durationData = 0;
		
		boolean afterSP = false;
		boolean readingHeader = true;
		
		recordName = decimalToText(decimalMSG, RECORDNAME_POS, recordNameLength + RECORDNAME_POS);
		
		appendHeader( output_description, ndefHeader, recordNameLength, payloadLength, recordName );
		
		i+= 3 + recordNameLength;
		
		while( i < decimalMSG.size() ) {
			
			if( readingHeader ) {
				
				if( afterSP ) {
					
					ndefHeader = decimalMSG.get( NDEFHEADER_POS + i );
					
					recordNameLength = decimalMSG.get( RECORDNAMELENGTH_POS + i);
					payloadLength = decimalMSG.get( PAYLOADLENGTH_POS + i);
					recordName = decimalToText(decimalMSG, RECORDNAME_POS + i, recordNameLength + RECORDNAME_POS + i);
					
					appendHeader( output_description, ndefHeader, recordNameLength, payloadLength, recordName );
				
					afterSP = false;

					i+= 3 + recordNameLength;
					
				}
		
				switch( recordName ) {
				
					case "Sp":
						
						afterSP = true;
						break;
						
					case "U":

						durationData = i + payloadLength - 1;
						output_description.append("Code abbreviation : ").append( decimalMSG.get(i) ).append("\n");		

						readingHeader = false;
						i++;
						break;
						
					case "T":
						
						durationData = i + payloadLength - 1;
						
						int statusByte = decimalMSG.get(i);
						i++;
						output_description.append("Status Byte : ").append( statusByte ).append("\n");
						output_description.append("ISO Language : ").append( decimalToText(decimalMSG, i , i + statusByte ) ).append("\n");
						
						
						readingHeader = false;
						i+=2;
						break;
						
					default:
						System.out.println("recordName error");
						break;
						
				}
			}
			
			else {
				
				if( i > durationData ) {

					output_description.append("\n");
					readingHeader = true;
					afterSP = true;

				}else {
				
					output_description.append( Character.toString( decimalMSG.get(i) ) );
					i++;

				}
			}
		}

		return output_description.toString();
		
	}
	
	static private ArrayList<Integer> parseHexa( String msg ) {
		
		ArrayList<Integer> parseTMP = new ArrayList<Integer>();
		
		for( int i = 0; i < msg.length() ; i+=2) {
			
			parseTMP.add( Integer.parseInt( msg.substring( i, i+2 ), 16) );
		}
		
		return parseTMP;
		
	}
	

	
	
}
