import java.io.IOException;

public class Main {
	
	public static void main(String[] args) throws IOException {
		
		Decoder decode = new Decoder();
		
		String decodedMSG = decode.decode( FileRead.readFile("ndef1.txt") );
		FileWrite.writeFile("output_ndef1.txt", decodedMSG);
		
		decodedMSG = decode.decode( FileRead.readFile("ndef2.txt") );
		FileWrite.writeFile("output_ndef2.txt", decodedMSG);
		
		decodedMSG = decode.decode( FileRead.readFile("ndef3.txt") );
		FileWrite.writeFile("output_ndef3.txt", decodedMSG);
		
		decodedMSG = decode.decode( FileRead.readFile("ndef4.txt") );
		FileWrite.writeFile("output_ndef4.txt", decodedMSG);
		
		
	}
	
	
}
