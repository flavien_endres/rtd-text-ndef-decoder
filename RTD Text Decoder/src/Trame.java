import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Trame {
	
	private String NDEF;
	
	public Trame( String NDEF ) {
		
		this.NDEF = Objects.requireNonNull( NDEF );
		
	}
	
	public String getNDEF() {
		
		return NDEF;
		
	}
	
	
	
}
