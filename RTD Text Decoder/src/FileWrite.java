import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileWrite {

	static public void writeFile( String filename, String decode ) throws IOException {
		
	    BufferedWriter writer = new BufferedWriter( new FileWriter( filename ) );
	    writer.write(decode);
	    writer.close();
	    
	}
	
}
