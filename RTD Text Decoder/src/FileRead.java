import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileRead {
	

	static public Trame readFile( String filename ) {
		
		String dataTMP = "";
		
		try {
			
			File myObj = new File(filename);
			Scanner myReader = new Scanner(myObj);
		    
			while (myReader.hasNextLine()) {
		    	  
		        dataTMP += myReader.nextLine();
		        
		    }
		    
				myReader.close();
		      
		} catch ( FileNotFoundException e ) {
		    	
			System.out.println("File reading error.");
		    e.printStackTrace();
		      
		}
		
		return new Trame( dataTMP );
		
		
	}
}
